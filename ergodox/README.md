# Ergodox linux instructions:
* Plug in keyboard
* Open a terminal
* May require root permissions (type: sudo bash, then type your password in)
* Install dfu-util package (pacman -S dfu-util or yum install dfu-util or apt-get install dfu-util or zypper install dfu-util)
* Press the flash button (orange led will turn on)
* Flash using dfu-util -D
* Orange led will turn off
* Keyboard is ready to go

Source(https://input.club/configurator-setup/#steplinux)

Configurator(https://input.club/configurator-ergodox/)
