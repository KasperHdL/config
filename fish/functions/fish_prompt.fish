function fish_prompt
#	set user (set_color --bold cyan)(whoami)(set_color normal)

#	set machine (set_color --bold blue)(hostname| cut -d . -f 1)(set_color normal)

	set -l realhome ~
	set directory (set_color --bold green)(echo $PWD | sed -e "s|^$realhome|~|")(set_color normal)

	set -g __fish_git_prompt_show_informative_status 'yes'
	set -g __fish_git_prompt_showcolorhints 'yes'
	set color_bold_red (set_color --bold red)
	set color_bold_yellow (set_color --bold yellow)
	set color_bold_cyan (set_color --bold cyan)
	set color_reset (set_color normal)
	set -g __fish_git_prompt_char_dirtystate "$color_bold_red±$color_reset"
	set -g __fish_git_prompt_char_cleanstate "$color_bold_red♥$color_reset"
	set -g __fish_git_prompt_char_stagedstate "$color_bold_yellow●$color_reset"
	set -g __fish_git_prompt_char_stagedstate "$color_bold_cyan…$color_reset"
	set -g __fish_git_prompt_char_stateseparator ' '
	# I have *no* idea why this works. Maybe set_color just ignores the second bold?
	set -g __fish_git_prompt_color_branch "--bold" "--bold" "yellow"
	set color_bold_magenta (set_color --bold magenta)
	set git (__fish_git_prompt "%s")

	echo -s $directory ' : ' $git 

	if [ $USER = "root" ]
		set caret (set_color red ) "#"
	else
		set caret (set_color magenta) \uf054
	end

	echo -s $caret ' '(set_color normal)



    set -l project

    if echo (pwd) | grep -qEi "^/Users/$USER/Projects/Sidetracked_Engine"
        set  project "Sidetracked_Engine"
    else if echo (pwd) | grep -qEi "^/Users/$USER/Projects/Vadir"
        set  project "Vadir"
    else if echo (pwd) | grep -qEi "^/Users/$USER/Projects/"
        set  project (echo (pwd) | sed "s#^/Users/$USER/Projects/\\([^/]*\\).*#\\1#")
    else if echo (pwd) | grep -qEi "^/Users/$USER/.config/"
        set  project ".config"
    else
        set  project "Terminal"
    end

end
