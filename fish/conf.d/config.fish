
#function fish_user_key_bindings
#  fish_vi_key_bindings
#  bind -M insert -m default jk backward-char force-repaint
#end
#fish_vi_key_bindings
fish_default_key_bindings

# Set variables
set --export VISUAL nvim
set --export EDITOR $VISUAL

# standard program aliases
alias ls="exa"
alias la="exa -la"
alias l="exa -l"

alias cat="bat"
alias vi="nvim"

alias sl="sl -a"
alias find="fzf"
alias cloc="tokei"

alias fopen="open (fzf)"
alias fvi="vi (fzf)"

# custom scripts
alias pacp="~/.config/scripts/pacp.sh"
alias cppcmd="~/.config/scripts/cppcmd.sh"
alias ccmd="~/.config/scripts/ccmd.sh"
alias brightness="~/.config/scripts/brightness.sh"

alias cmkclean="rm CMakeCache.txt; cmake"

alias mk="make -j 8"
alias cmk="cmake ..; make -j 8"
